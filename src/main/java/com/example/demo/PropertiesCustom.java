package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class PropertiesCustom {

    @Value("${gmail.credentialFilePath}")
    private String credentialFilePath;

    @Value("${gmail.tokenDirectoryPath}")
    private String tokenDirectoryPath;


    public String getCredentialFilePath() {
        return credentialFilePath;
    }

    public String getTokenDirectoryPath() {
        return tokenDirectoryPath;
    }
}
