package com.example.demo;

public class EmailBean {

    private String dateEmail = "";
    private String from = "";
    private String to = "";
    private String subject = "";
    private String emailBody = "";


    public EmailBean(){}

    public EmailBean(String dateEmail, String from, String to, String subject, String emailBody) {
        this.dateEmail = dateEmail;
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.emailBody = emailBody;
    }

    public String getDateEmail() {
        return dateEmail;
    }

    public void setDateEmail(String dateEmail) {
        this.dateEmail = dateEmail;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }
}
