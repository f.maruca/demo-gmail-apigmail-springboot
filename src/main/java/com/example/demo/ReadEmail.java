package com.example.demo;

import com.google.api.client.util.StringUtils;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ReadEmail {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private GmailApiConnection gmailApiConnection;

    public List<EmailBean> read() throws Exception{

        Gmail service = this.gmailApiConnection.start();

        // Print the labels in the user's account.
        String user = "me";

        //Gmail.Users.Messages.List request = service.users().messages().list(user).setQ(searchString);
        Gmail.Users.Messages.List request = service.users().messages().list(user);

        ListMessagesResponse listMsgResp = request.execute();
        request.setPageToken(listMsgResp.getNextPageToken());

        List<EmailBean> listEmailBeans = new ArrayList<EmailBean>();

        for(Message msg : listMsgResp.getMessages()){
            String messageId = msg.getId();
            Message message = service.users().messages().get(user, messageId).execute();
            //Message message = service.users().messages().get(user, messageId).setFormat("full").execute();

            EmailBean emailBean = this.getValue(message);
            listEmailBeans.add(emailBean);
        }


//        ListLabelsResponse listResponse = service.users().labels().list(user).execute();
//        List<com.google.api.services.gmail.model.Label> labels = listResponse.getLabels();
//        if (labels.isEmpty()) {
//            System.out.println("No labels found.");
//        } else {
//            System.out.println("Labels:");
//            for (Label label : labels) {
//                System.out.printf("- %s\n", label.getName());
//            }
//        }

        return listEmailBeans;
    }


    private EmailBean getValue(Message message){

        String dateEmail = "";
        String from = "";
        String to = "";
        String subject = "";
        String emailBody = "";

        List<MessagePartHeader> headers = message.getPayload().getHeaders();

        if (!headers.isEmpty()) {
            for (MessagePartHeader header : headers) {
                String name = header.getName();
                switch (name) {
                    case "From":
                        from = header.getValue();
                        break;
                    case "To":
                        to = header.getValue();
                        break;
                    case "Subject":
                        subject = header.getValue();
                        break;
                    case "Date":
                        dateEmail = header.getValue();
//                        if(date.contains(","))
//                            date = date.substring(date.indexOf(",") + 2,date.length());;
//                        String timestampFormat = "dd MMM yyyy HH:mm:ss Z";
                        //dataEmail = TimeUtils.fromFormattedString(timestampFormat,date) / 1000;
                        break;
                }
            }
        }


        // Print email body
        if (message.getPayload().getParts() != null && !message.getPayload().getParts().isEmpty()) {
            emailBody = StringUtils
                    .newStringUtf8(Base64.decodeBase64(message.getPayload().getParts().get(0).getBody().getData()));
        }else{
            emailBody = StringUtils
                    .newStringUtf8(Base64.decodeBase64(message.getPayload().getBody().getData()));

        }

        log.debug("");
        log.debug("");
        log.debug("====== START LOG VALUE EMAIL =====================================================");
        log.debug("");

        log.debug("Date: " + dateEmail);
        log.debug("From: " + from);
        log.debug("To: " + to);
        log.debug("Subject: " + subject);
        log.debug("Body: " + emailBody);


        EmailBean emailBean = new EmailBean(dateEmail, from, from, subject, emailBody);
        return emailBean;

    }
}
