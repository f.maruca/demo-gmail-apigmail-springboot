package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.example.demo")
@PropertySource("classpath:application.properties")
public class MainApplication implements CommandLineRunner {

    @Autowired
    private ReadEmail readEmail;

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        readEmail.read();
    }


}
